# Copyright 2020 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Basic image
ARG ALPINE_VERSION=3.12
ARG GO_VERSION=1.15.2
ARG GO_BASE_IMAGE=alpine${ALPINE_VERSION}
ARG GO_DOCKER_NAME=golang
ARG GO_DOCKER_TAG=${GO_VERSION}${GO_BASE_IMAGE:+-}${GO_BASE_IMAGE}

FROM ${GO_DOCKER_NAME}:${GO_DOCKER_TAG} as base

# Download, install and build packages
FROM base as builder

RUN \
    if command -v apt-get 2>&1 >/dev/null; then \
        apt-get update && \
        apt-get install --yes --no-install-recommends --no-upgrade \
            git \
            && \
        apt-get clean && \
        rm -rf /var/lib/apt/lists/*; \
    elif command -v apk 2>&1 >/dev/null; then \
        apk update --no-cache && \
        apk add --no-cache \
            git; \
    fi && \
    go get -u github.com/mikefarah/yq

FROM builder

COPY --from=builder /go/bin/* /go/bin/

RUN \
    if command -v apt-get 2>&1 >/dev/null; then \
        apt-get update && \
        apt-get install --yes --no-install-recommends --no-upgrade \
            shellcheck \
            && \
        apt-get clean && \
        rm -rf /var/lib/apt/lists/*; \
    elif command -v apk 2>&1 >/dev/null; then \
        apk update --no-cache && \
        apk add --no-cache \
            shellcheck; \
    fi
